package osgi.enroute.example.service.consumer;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import osgi.enroute.debug.api.Debug;
import osgi.enroute.example.api.MyService;
import osgi.enroute.rest.api.REST;

/**
 * This is the implementation. It consumes a MyService remote service.
 */
@Component(
	name="osgi.enroute.example.service.consumer"
)
public class MyRESTServiceConsumer implements REST {

	@Activate
	void activate(Map<String, Object> map) {
		System.out.println("[activate] MyRESTServiceConsumer");
	}

	@Deactivate
	void deactivate(Map<String, Object> map) {
		System.out.println("[deactivate] MyRESTServiceConsumer");
	}

	public String[] gettestme() {
		int index = 0;
		String[] result = new String[myServices.size()];
		for (MyService myService : myServices) {
			result[index++] = myService.sayHello();
		}
	    return result;
	}

	private final Set<MyService> myServices = ConcurrentHashMap.newKeySet();

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	void addMyService(MyService myService, Map<String, Object> map) {
	    myServices.add(myService);
	}

	void removeMyService(MyService myService, Map<String, Object> map) {
		myServices.remove(myService);
	}
}
