package osgi.enroute.example.service.consumer;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import osgi.enroute.debug.api.Debug;
import osgi.enroute.example.api.MyService;

/**
 * This is the implementation. It consumes a MyService remote service.
 */
@Component(
	service=MyCMDServiceConsumer.class,
	property = {
		Debug.COMMAND_SCOPE + "=cmd",
		Debug.COMMAND_FUNCTION + "=testme"
	},
	name="osgi.enroute.example.command.myserviceconsumer"
)
public class MyCMDServiceConsumer {

	@Activate
	void activate(Map<String, Object> map) {
		System.out.println("[activate] MyCMDServiceConsumer");
	}

	@Deactivate
	void deactivate(Map<String, Object> map) {
		System.out.println("[deactivate] MyCMDServiceConsumer");
	}

	public void testme() {
		for (MyService myService : myServices) {
			System.out.println("[language] " + myService.sayHello());
		}
	}

	private final Set<MyService> myServices = ConcurrentHashMap.newKeySet();

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	void addMyService(MyService myService, Map<String, Object> map) {
	    myServices.add(myService);
	}

	void removeMyService(MyService myService, Map<String, Object> map) {
		myServices.remove(myService);
	}

}
