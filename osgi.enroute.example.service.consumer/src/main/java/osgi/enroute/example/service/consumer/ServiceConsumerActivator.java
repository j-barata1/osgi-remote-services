package osgi.enroute.example.service.consumer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import osgi.enroute.configurer.api.RequireConfigurerExtender;

/**
 * 
 */
@RequireConfigurerExtender
public class ServiceConsumerActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("[start] ServiceConsumerActivator");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("[stop] ServiceConsumerActivator");
	}
}
