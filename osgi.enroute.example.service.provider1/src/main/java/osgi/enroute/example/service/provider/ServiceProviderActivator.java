package osgi.enroute.example.service.provider;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import osgi.enroute.configurer.api.RequireConfigurerExtender;

/**
 * 
 */
@RequireConfigurerExtender
public class ServiceProviderActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("[start] ServiceProviderActivator");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("[stop] ServiceProviderActivator");
	}
}
