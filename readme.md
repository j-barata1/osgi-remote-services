# OSGI EnRoute - Remote Services Example

## Build

The build requires Apache Maven to be installed:

 - $> wget http://mirror.olnevhost.net/pub/apache/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz
 - $> tar -zxvf apache-maven-3.5.2-bin.tar.gz
 - $> export M2_HOME=<installation directory>/apache-maven-3.5.2
 - $> export PATH=$PATH:$M2_HOME/bin

Then, clone this repository:
 - $> git clone https://gitlab.com/j-barata1/osgi-remote-services.git
 - $> cd osgi-remote-services

(The build requires that the bundle 'org.amdatu.remote.discovery.zookeeper-0.1.2.jar' is installed in your local maven repository. You can do that by launching the command '$> mvn validate').

Then, build it:
 - $> mvn package

## Configuration and Run

In order to test this example, you need to open four different shells.

Shell #1 (launch server) :
 - $> export ZOOKEEPER_HOST=localhost
 - $> export ZOOKEEPER_PORT=6789
 - $> java -jar <git repository root>/bndrun/zookeeper/generated/osgi.enroute.example.zookeeper.jar

Shell #2 (launch service provider1) :
 - $> export ZOOKEEPER_HOST=localhost
 - $> export ZOOKEEPER_PORT=6789
 - $> java -jar <git repository root>/bndrun/provider1/generated/osgi.enroute.example.provider1.jar

Shell #3 (launch service provider2) :
 - $> export ZOOKEEPER_HOST=localhost
 - $> export ZOOKEEPER_PORT=6789
 - $> java -jar <git repository root>/bndrun/provider2/generated/osgi.enroute.example.provider2.jar

Shell #4 (launch service consumer) :
 - $> export ZOOKEEPER_HOST=localhost
 - $> export ZOOKEEPER_PORT=6789
 - $> java -jar <git repository root>/bndrun/consumer/generated/osgi.enroute.example.consumer.jar

## Execute Test

In shell #4, you can type now the command :
 - 'g!> cmd:testme'

In Shell #2, you will get the trace :
 - g!> [sayHello] Guten Tag !

In Shell #3, you will get the trace :
 - g!> [sayHello] Konichiwa !

And finally, in Shell #4, you will get the trace :
 - g!> [activate] MyCMDServiceConsumer
 - g!> [language] German
 - g!> [language] Japanese
 - g!> [deactivate] MyCMDServiceConsumer
