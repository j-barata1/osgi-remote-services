#!/bin/sh
############################

export GITREPOSITORYPATH=`pwd`
export ZOOKEEPER_HOST=localhost
export ZOOKEEPER_PORT=6789
export DELAY_BETWEEN_COMMANDS=2

gnome-terminal --working-directory $GITREPOSITORYPATH --title 'Zookeeper server' -e 'java -jar bndrun/zookeeper/generated/osgi.enroute.example.zookeeper.jar'
sleep $DELAY_BETWEEN_COMMANDS
gnome-terminal --working-directory $GITREPOSITORYPATH --title 'Service provider 1' -e 'java -jar bndrun/provider1/generated/osgi.enroute.example.provider1.jar'
sleep $DELAY_BETWEEN_COMMANDS
gnome-terminal --working-directory $GITREPOSITORYPATH --title 'Service provider 2' -e 'java -jar bndrun/provider2/generated/osgi.enroute.example.provider2.jar'
sleep $DELAY_BETWEEN_COMMANDS
gnome-terminal --working-directory $GITREPOSITORYPATH --title 'Service consumer' -e 'java -jar bndrun/consumer/generated/osgi.enroute.example.consumer.jar'

