package osgi.enroute.example.service.provider;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import osgi.enroute.example.api.MyService;

/**
 * This is the implementation. It registers a MyService service.
 */
@Component(
	immediate=true,
	name="osgi.enroute.example.service.myservice2",
	property = {
		"service.exported.interfaces=*"
	}
)
public class MyServiceImpl2 implements MyService {

	@Activate
	void activate(Map<String, Object> map) {
		System.out.println("[activate] MyServiceImpl2");
	}

	@Deactivate
	void deactivate(Map<String, Object> map) {
		System.out.println("[deactivate] MyServiceImpl2");
	}

	@Override
	public String sayHello() {
		System.out.println("[sayHello] Konichiwa !");
		return "Japanese";
	}
}
