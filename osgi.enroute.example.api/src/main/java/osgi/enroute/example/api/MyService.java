package osgi.enroute.example.api;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface MyService {
  String sayHello();
}
