package osgi.enroute.example.command.provider;

import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import osgi.enroute.debug.api.Debug;
import osgi.enroute.example.api.MyService;

/**
 * This is the implementation. It registers the MyService interface and calls it through a Gogo command.
 */
@Component(
	service=MyServiceCommand.class,
	property = {
		Debug.COMMAND_SCOPE + "=cmd",
		Debug.COMMAND_FUNCTION + "=myservice"
	},
	name="osgi.enroute.example.command.myservice"
)
public class MyServiceCommand {
	private MyService target;

	public String sayHello() {
	    return "Hello";
	}

	@Reference
	void setMyService(MyService myService) {
		this.target = myService;
	}

}
